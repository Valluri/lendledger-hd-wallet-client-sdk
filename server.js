/**
 * @author Girijashankar Mishra
 * @version 1.0.0
 * @since 14-September-2018
 */

var hdWallet = require('stellar-hd-wallet');
var bip39 = require('bip39');
var sha256 = require('js-sha256').sha256;
var mysql = require('mysql');
const r2 = require("r2");

// var config = require('./config/config.json');


module.exports = AccountUsingMnemonic;


/**
 * @author Girijashankar Mishra
 * @description Create HD-Wallet using BIP39 protocol frm Stellar using mnemonic string.
 * @param {mnemonic} string 
 * @param {publicKey, secret} json 
 */
function AccountUsingMnemonic(mnemonic) {
    try {
        // If mnemonic hash does not exist in DB then create an account using mnemonic string.
        //Convert mnemonic string to SeedHex String using BIP39 protocol.
        var bip39hexSeed = bip39.mnemonicToSeedHex(mnemonic);

        //Generate HD-Wallet from SeedHex String obtained in previous step. 
        //Inside wallet you will get PublicKey and Secret 
        const wallet = hdWallet.fromSeed(bip39hexSeed);
        console.log('*******************************************************');
        console.log('mnemonic ====> ', mnemonic);

        console.log('wallet.getPublicKey(0) ====> ', wallet.getPublicKey(0));
        console.log('wallet.getSecret(0) ====> ', wallet.getSecret(0));
        console.log('wallet.getKeypair(0) ====> ', wallet.getKeypair(0));

        var output = {};
        output.accountId = wallet.getPublicKey(0);
        output.secret = wallet.getSecret(0);
        output.status = 'Mnemonic enrolled successfully in Federation Server.';
        output.remark = 'Do not share your secret with any one';
        console.log('*******************************************************');
        return output;

    } catch (error) {
        console.error(error);
        return error;
    }
}
